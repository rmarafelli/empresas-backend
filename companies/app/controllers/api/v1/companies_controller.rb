class Api::V1::CompaniesController < ApplicationController
  before_action :authenticate_api_v1_user!
  before_action :set_company, only: [:show, :update, :destroy]

  # GET /companies
  def index
    if filter_params[:name]
      @companies = Company.filter_by_name(filter_params[:name])
    else
      if filter_params[:company_type]
        @companies = Company.filter_by_type(filter_params[:company_type])
      else
        @companies = Company.all
      end
    end

    render json: @companies
  end

  # GET /companies/1
  def show
    render json: @company
  end

  # POST /companies
  def create
    @company = Company.new(company_params)

    if @company.save
      render json: @company, status: :created, location: @company
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /companies/1
  def update
    if @company.update(company_params)
      render json: @company
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  # DELETE /companies/1
  def destroy
    @company.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def company_params
      params.require(:company).permit(:name, :company_type)
    end

    def filter_params
      params.permit(:name, :company_type)
    end
end
