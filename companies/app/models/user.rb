class User < ApplicationRecord
  # Include default devise modules.

 devise :database_authenticatable, :recoverable,
 :trackable, :validatable, :registerable,
 :omniauthable
  include DeviseTokenAuth::Concerns::User
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  
end
