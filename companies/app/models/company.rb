class Company < ApplicationRecord
    scope :filter_by_type, -> (type) { where(company_type: type)}
    scope :filter_by_name, -> (name) { where(name: name)}

end
