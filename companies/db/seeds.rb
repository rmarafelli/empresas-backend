# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#companies = [{name: 'abc', type:1}, {name: 'def', type: 2}, {name: 'ghi', type: 3}]


Company.create!(name: 'empresa1', company_type: '1')
Company.create!(name: 'empresa2', company_type: '2')
Company.create!(name: 'empresa3', company_type: '3')


